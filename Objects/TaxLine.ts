/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface TaxLine {
    title: string;
    price: string;
    rate: number;
}
