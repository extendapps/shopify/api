/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Variant} from '../Objects/Variant';

export type VariantCallBack = (variant: Variant) => void;
export type VariantsCallBack = (variants: Variant[]) => void;
export type VariantsCountCallBack = (count: number) => void;

export interface VariantsPayload {
    variants: Variant[];
}

export interface VariantsCountPayload {
    count: number;
}

export interface VariantPayload {
    variant: Variant;
}
