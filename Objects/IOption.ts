/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface IOption {
    id?: number;
    product_id?: number;
    name: string;
    position?: number;
    values?: string[];
}
