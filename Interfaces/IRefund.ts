/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Refund} from '../Objects/Refund';

export type RefundCallBack = (refund: Refund) => void;
export type RefundsCallBack = (refunds: Refund[]) => void;

export interface RefundPayload {
    refund: Refund;
}

export interface RefundsPayload {
    refunds: Refund[];
}
