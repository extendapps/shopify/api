/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Shop {
    id: number;
    name: string;
    email: string;
    domain: string;
    province: string;
    country: string;
    address1: string;
    zip: string;
    city: string;
    source?: string;
    phone: string;
    latitude: number;
    longitude: number;
    primary_locale: string;
    address2: string;
    created_at: string; // '2012-02-15T15:12:21-05:00'
    updated_at: string; // '2012-02-15T15:12:21-05:00'
    country_code: string;
    country_name: string;
    currency: string;
    customer_email: string;
    timezone: string;
    iana_timezone: string;
    shop_owner: string;
    money_format: string;
    money_with_currency_format: string;
    weight_unit: string;
    province_code: string;
    taxes_included: boolean;
    tax_shipping?: string;
    county_taxes: boolean;
    plan_display_name: string;
    plan_name: string;
    has_discounts: boolean;
    has_gift_cards: boolean;
    myshopify_domain: string;
    google_apps_domain?: string;
    google_apps_login_enabled?: string;
    money_in_emails_format: string;
    money_with_currency_in_emails_format: string;
    eligible_for_payments: boolean;
    requires_extra_payments_agreement: boolean;
    password_enabled: boolean;
    has_storefront: boolean;
    eligible_for_card_reader_giveaway: boolean;
    finances: boolean;
    primary_location_id: number;
    checkout_api_supported: boolean;
    multi_location_enabled: boolean;
    setup_required: boolean;
    force_ssl: boolean;
    pre_launch_enabled: boolean;
}
