/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Address {
    id?: number;
    customer_id: number;
    first_name?: string;
    last_name?: string;
    company?: string;
    address1: string;
    address2?: string;
    city: string;
    province: string;
    country: string;
    zip: string;
    phone: string;
    name: string;
    province_code: string;
    country_code: string;
    country_name: string;
    default: boolean;
}

export interface LatLongAddress extends Address {
    latitude?: string;
    longitude?: string;
}
