/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {IImage} from '../Objects/IImage';

export type IImagesCallback = (images: IImage[]) => void;

export interface IImagesPayload {
    images: IImage[];
}
