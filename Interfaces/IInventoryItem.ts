/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {InventoryItem} from '../Objects/InventoryItem';

export interface CostPayload {
    inventory_item: {
        id: number;
        cost: string;
    };
}

export type InventoryItemCallBack = (inventory_item: InventoryItem) => void;

export interface UpdateInventoryItemOptions extends BaseOptions, CostPayload {
    OK: InventoryItemCallBack;
}
