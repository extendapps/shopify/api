/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Destination {
    id: number;
    address1: string;
    address2: string;
    city: string;
    company?: string;
    country: string;
    email: string;
    first_name: string;
    last_name: string;
    phone: string;
    province: string;
    zip: string;
}
