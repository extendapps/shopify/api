/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface DiscountCode {
    amount: string;
    code: string;
    type: 'fixed_amount' | 'percentage' | 'shipping';
}
