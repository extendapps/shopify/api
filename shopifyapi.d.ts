/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * KeyGen.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
import * as https from 'N/https';
import { CreateOrderTransactionOptions, GetOrderTransactionCountOptions, GetOrderTransactionOptions, GetOrderTransactionsOptions } from './Interfaces/IOrderTransaction';
import { CreateFulfillmentServiceOptions, DeleteFulfillmentServiceOptions, GetFulfillmentServiceOptions, GetFulfillmentServicesOptions, UpdateFulfillmentServiceOptions } from './Interfaces/IFulfillmentService';
import { CreateCustomerAddressOptions, GetCustomerAddressesOptions, GetCustomerAddressOptions, UpdateCustomerAddressOptions } from './Interfaces/ICustomerAddress';
import { CreateWebHookOptions, DeleteWebHookOptions, GetWebHookOptions, GetWebHooksOptions, UpdateWebHookOptions, ValidateWebHookOptions } from './Interfaces/IWebHook';
import { GetShopPropertiesOptions } from './Interfaces/IShop';
import { CreateProductVariantOptions, DeleteProductVariantOptions, GetProductVariantOptions, GetProductVariantsCountOptions, GetProductVariantsOptions, UpdateProductVariantOptions } from './Interfaces/IProductVariants';
import { GetProductImagesOptions } from './Interfaces/IProductImages';
import { CreateProductOptions, DeleteProductOptions, GetProductOptions, GetProductsOptions, UpdateProductOptions } from './Interfaces/IProduct';
import { CalculateOrderRefundOptions, GetOrderRefundOptions, GetOrderRefundsOptions } from './Interfaces/IOrderRefund';
import { GetOrderOptions, GetOrdersCountOptions, GetOrdersOptions } from './Interfaces/IOrder';
import { Order } from './Objects/Order';
import { Product } from './Objects/Product';
import { Customer } from './Objects/Customer';
import { GetLocationsOptions } from './Objects/Location';
import { AdjustInventoryLevelsOptions, ConnectInventoryLevelOptions, DeleteInventoryLevelOptions, GetInventoryLevelsOptions, SetInventoryLevelOptions } from './Interfaces/IInventoryLevel';
import { GetFulfillmentOrdersOptions } from './Interfaces/IFulfillmentOrder';
import { CompleteFulfillmentOptions, CreateFulfillmentOptions } from './Interfaces/IFulfillment';
import { UpdateInventoryItemOptions } from './Interfaces/IInventoryItem';
export declare class ShopifyAPI {
    private readonly apiKey;
    private readonly apiPassword;
    private readonly domainName;
    private readonly apiSecretGUID;
    private readonly apiVersion;
    constructor(apiKey: string, apiPassword: string, domainName: string, apiSecretGUID: string, apiVersion?: string);
    static decorateOrder(order: Order, shopfy: ShopifyAPI): void;
    static decorateCustomer(customer: Customer, shopfy: ShopifyAPI): void;
    static decorateProduct(product: Product, shopfy: ShopifyAPI): void;
    getFulfillmentOrders(options: GetFulfillmentOrdersOptions): void;
    createFulfillment(options: CreateFulfillmentOptions): void;
    completeFulfillment(options: CompleteFulfillmentOptions): void;
    getLocations(options: GetLocationsOptions): void;
    getOrders(options: GetOrdersOptions): void;
    getOrdersCount(options: GetOrdersCountOptions): void;
    getOrder(options: GetOrderOptions): void;
    getOrderRefunds(options: GetOrderRefundsOptions): void;
    getOrderRefund(options: GetOrderRefundOptions): void;
    calculateOrderRefund(options: CalculateOrderRefundOptions): void;
    getOrderTransactions(options: GetOrderTransactionsOptions): void;
    getOrderTransactionCount(options: GetOrderTransactionCountOptions): void;
    getOrderTransaction(options: GetOrderTransactionOptions): void;
    createOrderTransaction(options: CreateOrderTransactionOptions): void;
    updateProduct(options: UpdateProductOptions): void;
    createProduct(options: CreateProductOptions): void;
    getProducts(options: GetProductsOptions): void;
    getProduct(options: GetProductOptions): void;
    deleteProduct(options: DeleteProductOptions): void;
    getProductImages(options: GetProductImagesOptions): void;
    getProductVariants(options: GetProductVariantsOptions): void;
    getProductVariantsCount(options: GetProductVariantsCountOptions): void;
    getProductVariant(options: GetProductVariantOptions): void;
    updateProductVariant(options: UpdateProductVariantOptions): void;
    createProductVariant(options: CreateProductVariantOptions): void;
    deleteProductVariant(options: DeleteProductVariantOptions): void;
    getShopProperties(options: GetShopPropertiesOptions): void;
    validateWebHook(options: ValidateWebHookOptions): void;
    getWebHook(options: GetWebHookOptions): void;
    getWebHooks(options: GetWebHooksOptions): void;
    createWebHook(options: CreateWebHookOptions): void;
    updateWebHook(options: UpdateWebHookOptions): void;
    deleteWebHook(options: DeleteWebHookOptions): void;
    getCustomerAddresses(options: GetCustomerAddressesOptions): void;
    getCustomerAddress(options: GetCustomerAddressOptions): void;
    createCustomerAddress(options: CreateCustomerAddressOptions): void;
    updateCustomerAddress(options: UpdateCustomerAddressOptions): void;
    getFulfillmentServices(options: GetFulfillmentServicesOptions): void;
    createFulfillmentService(options: CreateFulfillmentServiceOptions): void;
    getFulfillmentService(options: GetFulfillmentServiceOptions): void;
    updateFulfillmentService(options: UpdateFulfillmentServiceOptions): void;
    updateInventoryItem(options: UpdateInventoryItemOptions): void;
    deleteFulfillmentService(options: DeleteFulfillmentServiceOptions): void;
    getInventoryLevels(options: GetInventoryLevelsOptions): void;
    adjustInventoryLevel(options: AdjustInventoryLevelsOptions): void;
    deleteInventoryLevel(options: DeleteInventoryLevelOptions): void;
    connectInventoryLevel(options: ConnectInventoryLevelOptions): void;
    setInventoryLevel(options: SetInventoryLevelOptions): void;
    private makeDELETERequest;
    private makeGETRequest;
    private makePOSTRequest;
    private makePUTRequest;
    private makeRequest;
}
export type ShopifyCallBack = (clientResponse: https.ClientResponse) => void;
