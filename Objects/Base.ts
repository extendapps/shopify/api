/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Base {
    name?: string;
    id: number;
    created_at: string; // '2012-02-15T15:12:21-05:00'
}
