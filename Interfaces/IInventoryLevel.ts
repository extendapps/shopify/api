/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {InventoryLevel} from '../Objects/InventoryLevel';

// tslint:disable-next-line:no-empty-interface
export type InventoryLevelBaseOptions = BaseOptions;

export interface GetInventoryLevelsOptions extends InventoryLevelBaseOptions {
    inventory_item_ids?: number[];
    location_ids?: number[];
    limit?: number; // (default: 50, maximum: 250)
    updated_at_min?: string; // Show inventory levels updated at or after date (format: 2019-03-19T01:21:44-04:00).
    OK: GetInventoryLevelCallback;
}

export interface AdjustInventoryLevelsOptions extends InventoryLevelBaseOptions {
    inventory_item_id: number;
    location_id: number;
    available_adjustment: number;
    OK: InventoryLevelCallback;
}

export interface DeleteInventoryLevelOptions extends InventoryLevelBaseOptions {
    inventory_item_id: number;
    location_id: number;
    Deleted: InventoryLevelDeleteCallback;
}

export interface ConnectInventoryLevelOptions extends InventoryLevelBaseOptions {
    inventory_item_id: number;
    location_id: number;
    relocate_if_necessary: boolean; // Default: false
    Created: InventoryLevelCallback;
}

export interface SetInventoryLevelOptions extends InventoryLevelBaseOptions {
    inventory_item_id: number;
    location_id: number;
    available: number;
    disconnect_if_necessary: boolean; // Default: false
    OK: InventoryLevelCallback;
}

export type InventoryLevelCallback = (inventoryLevel: InventoryLevel) => void;

export interface InventoryLevelResponse {
    inventory_level: InventoryLevel;
}

export type GetInventoryLevelCallback = (inventoryLevels: InventoryLevel[]) => void;

export interface GetInventoryLevelResponse {
    inventory_levels: InventoryLevel[];
}

export type InventoryLevelDeleteCallback = () => void;
