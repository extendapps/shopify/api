/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Receipt {
    testcase: boolean;
    authorization: string;
    gift_card_id?: string;
    gift_card_last_characters?: string;
}
