/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {IImagesCallback} from './IImage';

export interface GetContextProductImagesOptions extends BaseOptions {
    OK: IImagesCallback;
}

export interface GetProductImagesOptions extends GetContextProductImagesOptions {
    product_id: number;
}

export type GetContextProductImagesFunction = (options: GetContextProductImagesOptions) => void;
