/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';

export interface CustomerBaseOptions extends BaseOptions {
    customerId: number;
}
