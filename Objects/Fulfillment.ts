/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {LineItem} from './LineItem';
import {Base} from './Base';

export interface Fulfillment extends Base {
    line_items: LineItem[];
    location_id: number;
    notify_customer: boolean;
    order_id: number;
    origin_address: OriginAddress[];
    receipt: Receipt;
    service: string;
    shipment_status: string;
    status: string;
    tracking_company: string;
    tracking_numbers: string[];
    tracking_urls: string[];
    updated_at: Date;
    variant_inventory_management: string;
}

export interface Receipt {
    testcase: boolean;
    authorization: string;
}

export interface OriginAddress {
    address1: string;
    address2: string;
    city: string;
    country_code: string;
    province_code: string;
    zip: string;
}
