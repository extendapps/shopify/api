/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * KeyGen.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */
define(["require", "exports", "N/https", "N/crypto", "N/log"], function (require, exports, https, crypto, log) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ShopifyAPI = void 0;
    // noinspection JSUnusedGlobalSymbols,DuplicatedCode
    var ShopifyAPI = /** @class */ (function () {
        function ShopifyAPI(apiKey, apiPassword, domainName, apiSecretGUID, apiVersion) {
            if (apiVersion === void 0) { apiVersion = '2023-01'; }
            this.apiKey = apiKey;
            this.apiPassword = apiPassword;
            this.domainName = domainName;
            this.apiSecretGUID = apiSecretGUID;
            this.apiVersion = apiVersion;
        }
        ShopifyAPI.decorateOrder = function (order, shopfy) {
            order.getTransaction = function (options) {
                options.id = order.id;
                shopfy.getOrderTransaction(options);
            };
            order.getTransactions = function (options) {
                options.id = order.id;
                shopfy.getOrderTransactions(options);
            };
            order.getTransactionCount = function (options) {
                options.id = order.id;
                shopfy.getOrderTransactionCount(options);
            };
            order.createTransaction = function (options) {
                options.id = order.id;
                shopfy.createOrderTransaction(options);
            };
            order.getRefund = function (options) {
                options.id = order.id;
                shopfy.getOrderRefund(options);
            };
            order.getRefunds = function (options) {
                options.id = order.id;
                shopfy.getOrderRefunds(options);
            };
        };
        ShopifyAPI.decorateCustomer = function (customer, shopfy) {
            customer.getAddress = function (options) {
                options.customerId = customer.id;
                shopfy.getCustomerAddress(options);
            };
            customer.getAddresses = function (options) {
                options.customerId = customer.id;
                shopfy.getCustomerAddresses(options);
            };
            customer.createAddress = function (options) {
                options.customerId = customer.id;
                shopfy.createCustomerAddress(options);
            };
            customer.updateAddress = function (options) {
                options.customerId = customer.id;
                shopfy.updateCustomerAddress(options);
            };
        };
        ShopifyAPI.decorateProduct = function (product, shopfy) {
            product.getVariants = function (options) {
                options.product_id = product.id;
                shopfy.getProductVariants(options);
            };
            product.getImages = function (options) {
                options.product_id = product.id;
                shopfy.getProductImages(options);
            };
            product.getVariantsCount = function (options) {
                options.product_id = product.id;
                shopfy.getProductVariantsCount(options);
            };
        };
        ShopifyAPI.prototype.getFulfillmentOrders = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.order_id, "/fulfillment_orders.json"),
                OK: function (clientResponse) {
                    var getFulfillmentOrdersResponse = JSON.parse(clientResponse.body);
                    options.OK(getFulfillmentOrdersResponse.fulfillment_orders);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getFulfillmentOrders', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createFulfillment = function (options) {
            this.makePOSTRequest({
                resource: "fulfillments.json",
                payload: {
                    fulfillment: options.payload,
                },
                Created: function (clientResponse) {
                    var fulfillmentResponse = JSON.parse(clientResponse.body);
                    options.Created(fulfillmentResponse.fulfillment);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createFulfillment', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.completeFulfillment = function (options) {
            this.makePOSTRequest({
                resource: "orders/".concat(options.order_id, "/fulfillments/").concat(options.fulfillment_id, "/complete.json"),
                payload: {},
                Created: function (clientResponse) {
                    var fulfillmentResponse = JSON.parse(clientResponse.body);
                    options.Created(fulfillmentResponse.fulfillment);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('completeFulfillment', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getLocations = function (options) {
            this.makeGETRequest({
                resource: "locations.json",
                OK: function (clientResponse) {
                    var getLocationsResponse = JSON.parse(clientResponse.body);
                    options.OK(getLocationsResponse.locations);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getLocations', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrders = function (options) {
            var _this = this;
            var getOrdersURL = "orders.json";
            var filters = [];
            if (options.ids) {
                filters.push("ids=".concat(options.ids.join(',')));
            }
            if (options.limit) {
                filters.push("limit=".concat(options.limit));
            }
            if (options.since_id) {
                filters.push("since_id=".concat(options.since_id));
            }
            if (options.created_at_min) {
                filters.push("created_at_min=".concat(options.created_at_min));
            }
            if (options.created_at_max) {
                filters.push("created_at_max=".concat(options.created_at_max));
            }
            if (options.updated_at_min) {
                filters.push("updated_at_min=".concat(options.updated_at_min));
            }
            if (options.updated_at_max) {
                filters.push("updated_at_max=".concat(options.updated_at_max));
            }
            if (options.processed_at_min) {
                filters.push("processed_at_min=".concat(options.processed_at_min));
            }
            if (options.processed_at_max) {
                filters.push("processed_at_max=".concat(options.processed_at_max));
            }
            if (options.attribution_app_id) {
                filters.push("attribution_app_id=".concat(options.attribution_app_id));
            }
            if (options.status) {
                filters.push("status=".concat(options.status));
            }
            if (options.financial_status) {
                filters.push("financial_status=".concat(options.financial_status));
            }
            if (options.fulfillment_status) {
                filters.push("fulfillment_status=".concat(options.fulfillment_status));
            }
            if (options.fields) {
                filters.push("fields=".concat(options.fields.join(',')));
            }
            if (filters.length > 0) {
                getOrdersURL = "".concat(getOrdersURL, "?").concat(filters.join('&'));
            }
            if (options.order) {
                getOrdersURL = "".concat(getOrdersURL, "&order=").concat(options.order);
            }
            this.makeGETRequest({
                resource: getOrdersURL,
                OK: function (clientResponse) {
                    var getOrdersResponse = JSON.parse(clientResponse.body);
                    for (var _i = 0, _a = getOrdersResponse.orders; _i < _a.length; _i++) {
                        var order = _a[_i];
                        ShopifyAPI.decorateOrder(order, _this);
                    }
                    options.OK(getOrdersResponse.orders);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrders', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrdersCount = function (options) {
            var getOrdersCountURL = "orders/count.json";
            var filters = [];
            if (options.ids) {
                filters.push("ids=".concat(options.ids.join(',')));
            }
            if (options.limit) {
                filters.push("limit=".concat(options.limit));
            }
            if (options.since_id) {
                filters.push("since_id=".concat(options.since_id));
            }
            if (options.created_at_min) {
                filters.push("created_at_min=".concat(options.created_at_min));
            }
            if (options.created_at_max) {
                filters.push("created_at_max=".concat(options.created_at_max));
            }
            if (options.updated_at_min) {
                filters.push("updated_at_min=".concat(options.updated_at_min));
            }
            if (options.updated_at_max) {
                filters.push("updated_at_max=".concat(options.updated_at_max));
            }
            if (options.processed_at_min) {
                filters.push("processed_at_min=".concat(options.processed_at_min));
            }
            if (options.processed_at_max) {
                filters.push("processed_at_max=".concat(options.processed_at_max));
            }
            if (options.attribution_app_id) {
                filters.push("attribution_app_id=".concat(options.attribution_app_id));
            }
            if (options.status) {
                filters.push("status=".concat(options.status));
            }
            if (options.financial_status) {
                filters.push("financial_status=".concat(options.financial_status));
            }
            if (options.fulfillment_status) {
                filters.push("fulfillment_status=".concat(options.fulfillment_status));
            }
            if (options.fields) {
                filters.push("fields=".concat(options.fields.join(',')));
            }
            if (filters.length > 0) {
                getOrdersCountURL = "".concat(getOrdersCountURL, "?").concat(filters.join('&'));
            }
            this.makeGETRequest({
                resource: getOrdersCountURL,
                OK: function (clientResponse) {
                    var getOrdersCountResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrdersCountResponse.count);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getORdersCount', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrder = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.id, ".json"),
                OK: function (clientResponse) {
                    var getOrderResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrderResponse.order);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrder', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrderRefunds = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.id, "/refunds.json"),
                OK: function (clientResponse) {
                    var getOrderRefundsResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrderRefundsResponse.refunds);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrderRefunds', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrderRefund = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.id, "/refunds/").concat(options.refundId, ".json"),
                OK: function (clientResponse) {
                    var getOrderRefundResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrderRefundResponse.refund);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrderRefund', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.calculateOrderRefund = function (options) {
            this.makeRequest({
                method: https.Method.POST,
                resource: "orders/".concat(options.id, "/refunds/calculate.json"),
                payload: {
                    refund: options.refund,
                },
                OK: function (clientResponse) {
                    var calculateOrderRefundResponse = JSON.parse(clientResponse.body);
                    options.OK(calculateOrderRefundResponse.refund);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('calculateOrderRefund', clientResponse);
                    }
                },
                retryCount: 0,
            });
        };
        ShopifyAPI.prototype.getOrderTransactions = function (options) {
            var getOrderTransactionsURL = "orders/".concat(options.id, "/transactions.json");
            var extraParams = [];
            if (options.since_id) {
                extraParams.push("since_id=".concat(options.since_id));
            }
            if (options.fields) {
                extraParams.push("fields=".concat(options.fields));
            }
            if (extraParams.length > 0) {
                getOrderTransactionsURL = "".concat(getOrderTransactionsURL, "?").concat(extraParams.join('&'));
            }
            this.makeGETRequest({
                resource: getOrderTransactionsURL,
                OK: function (clientResponse) {
                    var orderTransactionsResponse = JSON.parse(clientResponse.body);
                    options.OK(orderTransactionsResponse.transactions);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrderTransactions', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrderTransactionCount = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.id, "/transactions/count.json"),
                OK: function (clientResponse) {
                    var getOrderTransactionCountResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrderTransactionCountResponse.count);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrderTransactionCount', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getOrderTransaction = function (options) {
            this.makeGETRequest({
                resource: "orders/".concat(options.id, "/transactions/").concat(options.transactionId, ".json"),
                OK: function (clientResponse) {
                    var getOrderTransactionResponse = JSON.parse(clientResponse.body);
                    options.OK(getOrderTransactionResponse.transaction);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getOrderTransaction', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createOrderTransaction = function (options) {
            this.makePOSTRequest({
                resource: "orders/".concat(options.id, "/transactions.json"),
                payload: {
                    transaction: options.transaction,
                },
                Created: function (clientResponse) {
                    var createOrderTransactionResponse = JSON.parse(clientResponse.body);
                    options.Created(createOrderTransactionResponse.transaction);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createOrderTransaction', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateProduct = function (options) {
            var _this = this;
            this.makePUTRequest({
                resource: "products/".concat(options.product.id, ".json"),
                payload: {
                    product: options.product,
                },
                OK: function (clientResponse) {
                    var productResponse = JSON.parse(clientResponse.body);
                    ShopifyAPI.decorateProduct(productResponse.product, _this);
                    options.OK(productResponse.product);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateProduct', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createProduct = function (options) {
            var _this = this;
            this.makePOSTRequest({
                resource: "products.json",
                payload: {
                    product: options.product,
                },
                Created: function (clientResponse) {
                    var productResponse = JSON.parse(clientResponse.body);
                    ShopifyAPI.decorateProduct(productResponse.product, _this);
                    options.Created(productResponse.product);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createProduct', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProducts = function (options) {
            var _this = this;
            var getProductsURL = "products.json";
            var filters = [];
            if (options.ids) {
                filters.push("ids=".concat(options.ids.join(',')));
            }
            if (options.limit) {
                filters.push("limit=".concat(options.limit));
            }
            if (options.since_id) {
                filters.push("since_id=".concat(options.since_id));
            }
            if (options.created_at_min) {
                filters.push("created_at_min=".concat(options.created_at_min));
            }
            if (options.created_at_max) {
                filters.push("created_at_max=".concat(options.created_at_max));
            }
            if (options.updated_at_min) {
                filters.push("updated_at_min=".concat(options.updated_at_min));
            }
            if (options.updated_at_max) {
                filters.push("updated_at_max=".concat(options.updated_at_max));
            }
            if (options.title) {
                filters.push("title=".concat(options.title));
            }
            if (options.vendor) {
                filters.push("vendor=".concat(options.vendor));
            }
            if (options.handle) {
                filters.push("handle=".concat(options.handle));
            }
            if (options.product_type) {
                filters.push("product_type=".concat(options.product_type));
            }
            if (options.collection_id) {
                filters.push("collection_id=".concat(options.collection_id));
            }
            if (options.fields) {
                filters.push("fields=".concat(options.fields.join(',')));
            }
            if (filters.length > 0) {
                getProductsURL = "".concat(getProductsURL, "?").concat(filters.join('&'));
            }
            this.makeGETRequest({
                resource: getProductsURL,
                OK: function (clientResponse) {
                    var productsResponse = JSON.parse(clientResponse.body);
                    for (var _i = 0, _a = productsResponse.products; _i < _a.length; _i++) {
                        var product = _a[_i];
                        ShopifyAPI.decorateProduct(product, _this);
                    }
                    options.OK(productsResponse.products);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProducts', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProduct = function (options) {
            var _this = this;
            this.makeGETRequest({
                resource: "products/".concat(options.product_id, ".json"),
                OK: function (clientResponse) {
                    var productResponse = JSON.parse(clientResponse.body);
                    ShopifyAPI.decorateProduct(productResponse.product, _this);
                    options.OK(productResponse.product);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProduct', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.deleteProduct = function (options) {
            this.makeDELETERequest({
                resource: "products/".concat(options.product_id, ".json"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('deleteProduct', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProductImages = function (options) {
            this.makeGETRequest({
                resource: "products/".concat(options.product_id, "/images.json"),
                OK: function (clientResponse) {
                    var getProductImagesResponse = JSON.parse(clientResponse.body);
                    options.OK(getProductImagesResponse.images);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProductImages', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProductVariants = function (options) {
            this.makeGETRequest({
                resource: "products/".concat(options.product_id, "/variants.json"),
                OK: function (clientResponse) {
                    var productVariantsResponse = JSON.parse(clientResponse.body);
                    options.OK(productVariantsResponse.variants);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProductVariants', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProductVariantsCount = function (options) {
            this.makeGETRequest({
                resource: "products/".concat(options.product_id, "/variants/count.json"),
                OK: function (clientResponse) {
                    var productVariantsCountResponse = JSON.parse(clientResponse.body);
                    options.OK(productVariantsCountResponse.count);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProductVariants', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getProductVariant = function (options) {
            this.makeGETRequest({
                resource: "variants/".concat(options.variant_id, ".json"),
                OK: function (clientResponse) {
                    var getProductVariantResponse = JSON.parse(clientResponse.body);
                    options.OK(getProductVariantResponse.variant);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getProductVariant', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateProductVariant = function (options) {
            this.makePUTRequest({
                resource: "variants/".concat(options.variant.id, ".json"),
                payload: {
                    variant: options.variant,
                },
                OK: function (clientResponse) {
                    var updateProductVariantResponse = JSON.parse(clientResponse.body);
                    options.OK(updateProductVariantResponse.variant);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateProductVariant', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createProductVariant = function (options) {
            this.makePOSTRequest({
                resource: "products/".concat(options.variant.product_id, "/variants.json"),
                payload: {
                    variant: options.variant,
                },
                Created: function (clientResponse) {
                    var createProductVariantResponse = JSON.parse(clientResponse.body);
                    options.Created(createProductVariantResponse.variant);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createProductVariant', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.deleteProductVariant = function (options) {
            this.makeDELETERequest({
                resource: "products/".concat(options.product_id, "/variants/").concat(options.variant_id),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('deleteProductVariant', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getShopProperties = function (options) {
            this.makeGETRequest({
                resource: "shop.json",
                OK: function (clientResponse) {
                    var setShopPropertiesResponse = JSON.parse(clientResponse.body);
                    options.OK(setShopPropertiesResponse.shop);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getShopProperties', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.validateWebHook = function (options) {
            if (options.hmacHeader === null) {
                options.Validated();
            }
            else {
                var hmacSHA256 = crypto.createHmac({
                    algorithm: crypto.HashAlg.SHA256,
                    key: crypto.createSecretKey({
                        guid: this.apiSecretGUID,
                        encoding: crypto.Encoding.UTF_8,
                    }),
                });
                hmacSHA256.update({
                    input: typeof options.webHookData === 'object' ? JSON.stringify(options.webHookData) : options.webHookData,
                    inputEncoding: crypto.Encoding.UTF_8,
                });
                var calculatedHMAC = hmacSHA256.digest({
                    outputEncoding: crypto.Encoding.BASE_64,
                });
                // Setting the hmacHeader to null indicates we want to skip validation
                if (options.hmacHeader === calculatedHMAC) {
                    options.Validated();
                }
                else {
                    options.Failed();
                }
            }
        };
        ShopifyAPI.prototype.getWebHook = function (options) {
            this.makeGETRequest({
                resource: "webhooks/".concat(options.webhookId, ".json"),
                OK: function (clientResponse) {
                    var getWebHookResponse = JSON.parse(clientResponse.body);
                    options.OK(getWebHookResponse.webhook);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getWebHook', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getWebHooks = function (options) {
            this.makeGETRequest({
                resource: "webhooks.json",
                OK: function (clientResponse) {
                    var getWebHooksResponse = JSON.parse(clientResponse.body);
                    options.OK(getWebHooksResponse.webhooks);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getWebHooks', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createWebHook = function (options) {
            this.makePOSTRequest({
                resource: "webhooks.json",
                payload: {
                    webhook: options.webhook,
                },
                Created: function (clientResponse) {
                    var createtWebHookResponse = JSON.parse(clientResponse.body);
                    options.Created(createtWebHookResponse.webhook);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createWebHook', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateWebHook = function (options) {
            this.makePUTRequest({
                resource: "webhooks/".concat(options.webhook.id, ".json"),
                payload: {
                    webhook: options.webhook,
                },
                OK: function (clientResponse) {
                    var updatetWebHookResponse = JSON.parse(clientResponse.body);
                    options.OK(updatetWebHookResponse.webhook);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateWebHook', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.deleteWebHook = function (options) {
            this.makeDELETERequest({
                resource: "webhooks/".concat(options.webhook.id, ".json"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('deleteWebHook', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getCustomerAddresses = function (options) {
            this.makeGETRequest({
                resource: "customers/".concat(options.customerId, "/addresses.json"),
                OK: function (clientResponse) {
                    var getCustomerAddressesResponse = JSON.parse(clientResponse.body);
                    options.OK(getCustomerAddressesResponse.addresses);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getCustomerAddresses', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getCustomerAddress = function (options) {
            this.makeGETRequest({
                resource: "customers/".concat(options.customerId, "/addresses/").concat(options.addressId, ".json"),
                OK: function (clientResponse) {
                    var getCustomerAddressResponse = JSON.parse(clientResponse.body);
                    options.OK(getCustomerAddressResponse.customer_address);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getCustomerAddress', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createCustomerAddress = function (options) {
            this.makePOSTRequest({
                resource: "customers/".concat(options.customerId, "/addresses.json"),
                payload: {
                    address: options.address,
                },
                Created: function (clientResponse) {
                    var customerAddressResponse = JSON.parse(clientResponse.body);
                    options.Created(customerAddressResponse.customer_address);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createCustomerAddress', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateCustomerAddress = function (options) {
            this.makePUTRequest({
                resource: "customers/".concat(options.customerId, "/addresses/").concat(options.address.id, ".json"),
                payload: {
                    address: options.address,
                },
                OK: function (clientResponse) {
                    var customerAddressResponse = JSON.parse(clientResponse.body);
                    options.OK(customerAddressResponse.customer_address);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateCustomerAddress', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getFulfillmentServices = function (options) {
            this.makeGETRequest({
                resource: "fulfillment_services.json",
                OK: function (clientResponse) {
                    var getFulfillmentServicesResponse = JSON.parse(clientResponse.body);
                    options.OK(getFulfillmentServicesResponse.fulfillment_services);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getFulfillmentServices', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.createFulfillmentService = function (options) {
            this.makePOSTRequest({
                resource: "fulfillment_services.json",
                payload: {
                    fulfillment_service: options.fulfillment_service,
                },
                Created: function (clientResponse) {
                    var createFulfillmentServiceResponse = JSON.parse(clientResponse.body);
                    options.Created(createFulfillmentServiceResponse.fulfillment_service);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('createFulfillmentService', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getFulfillmentService = function (options) {
            this.makeGETRequest({
                resource: "fulfillment_services/".concat(options.id, ".json"),
                OK: function (clientResponse) {
                    var getFulfillmentServiceResponse = JSON.parse(clientResponse.body);
                    options.OK(getFulfillmentServiceResponse.fulfillment_service);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getFulfillmentService', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateFulfillmentService = function (options) {
            this.makePUTRequest({
                resource: "fulfillment_services/".concat(options.fulfillment_service.id, ".json"),
                payload: {
                    fulfillment_service: options.fulfillment_service,
                },
                OK: function (clientResponse) {
                    var updateFulfillmentServiceResponse = JSON.parse(clientResponse.body);
                    options.OK(updateFulfillmentServiceResponse.fulfillment_service);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateFulfillmentService', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.updateInventoryItem = function (options) {
            this.makePUTRequest({
                resource: "inventory_items/".concat(options.inventory_item.id, ".json"),
                payload: {
                    inventory_item: options.inventory_item,
                },
                OK: function (clientResponse) {
                    var updateResponse = JSON.parse(clientResponse.body);
                    options.OK(updateResponse.inventory_item);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('updateFulfillmentService', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.deleteFulfillmentService = function (options) {
            this.makeDELETERequest({
                resource: "fulfillment_services/".concat(options.id, ".json"),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('deleteFulfillmentService', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.getInventoryLevels = function (options) {
            var getInventoryLevelsURL = "inventory_levels.json";
            var filters = [];
            if (options.inventory_item_ids) {
                filters.push("inventory_item_ids=".concat(options.inventory_item_ids.join(',')));
            }
            if (options.location_ids) {
                filters.push("location_ids=".concat(options.location_ids.join(',')));
            }
            if (options.limit) {
                filters.push("limit=".concat(options.limit));
            }
            if (options.updated_at_min) {
                filters.push("updated_at_min=".concat(options.updated_at_min));
            }
            if (filters.length > 0) {
                getInventoryLevelsURL = "".concat(getInventoryLevelsURL, "?").concat(filters.join('&'));
            }
            this.makeGETRequest({
                resource: getInventoryLevelsURL,
                OK: function (clientResponse) {
                    var getInventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.OK(getInventoryLevelResponse.inventory_levels);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('getInventoryLevels', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.adjustInventoryLevel = function (options) {
            this.makePOSTRequest({
                resource: "inventory_levels/adjust.json",
                payload: {
                    location_id: options.location_id,
                    inventory_item_id: options.inventory_item_id,
                    available_adjustment: options.available_adjustment,
                },
                OK: function (clientResponse) {
                    var inventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.OK(inventoryLevelResponse.inventory_level);
                },
                Created: function (clientResponse) {
                    var inventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.OK(inventoryLevelResponse.inventory_level);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('adjustInventoryLevel', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.deleteInventoryLevel = function (options) {
            this.makeDELETERequest({
                resource: "inventory_levels.json?inventory_item_id=".concat(options.inventory_item_id, "&location_id=").concat(options.location_id),
                Deleted: function () {
                    options.Deleted();
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('deleteInventoryLevel', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.connectInventoryLevel = function (options) {
            this.makePOSTRequest({
                resource: "inventory_levels/connect.json",
                payload: {
                    location_id: options.location_id,
                    inventory_item_id: options.inventory_item_id,
                    relocate_if_necessary: options.relocate_if_necessary,
                },
                Created: function (clientResponse) {
                    var inventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.Created(inventoryLevelResponse.inventory_level);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('connectInventoryLevel', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.setInventoryLevel = function (options) {
            this.makePOSTRequest({
                resource: "inventory_levels/set.json",
                payload: {
                    location_id: options.location_id,
                    inventory_item_id: options.inventory_item_id,
                    available: options.available,
                    disconnect_if_necessary: options.disconnect_if_necessary,
                },
                OK: function (clientResponse) {
                    var inventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.OK(inventoryLevelResponse.inventory_level);
                },
                Created: function (clientResponse) {
                    var inventoryLevelResponse = JSON.parse(clientResponse.body);
                    options.OK(inventoryLevelResponse.inventory_level);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    else {
                        log.error('setInventoryLevel', clientResponse);
                    }
                },
            });
        };
        ShopifyAPI.prototype.makeDELETERequest = function (options) {
            this.makeRequest({
                method: https.Method.DELETE,
                resource: options.resource,
                Deleted: function (clientResponse) {
                    options.Deleted(clientResponse);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                },
                retryCount: 0,
            });
        };
        ShopifyAPI.prototype.makeGETRequest = function (options) {
            this.makeRequest({
                method: https.Method.GET,
                resource: options.resource,
                OK: function (clientResponse) {
                    options.OK(clientResponse);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                },
                retryCount: 0,
            });
        };
        ShopifyAPI.prototype.makePOSTRequest = function (options) {
            this.makeRequest({
                method: https.Method.POST,
                resource: options.resource,
                payload: options.payload,
                OK: function (clientResponse) {
                    if (options.OK) {
                        options.OK(clientResponse);
                    }
                },
                Created: function (clientResponse) {
                    options.Created(clientResponse);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                },
                retryCount: 0,
            });
        };
        ShopifyAPI.prototype.makePUTRequest = function (options) {
            this.makeRequest({
                method: https.Method.PUT,
                resource: options.resource,
                payload: options.payload,
                OK: function (clientResponse) {
                    options.OK(clientResponse);
                },
                Failed: function (clientResponse) {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                },
                retryCount: 0,
            });
        };
        ShopifyAPI.prototype.makeRequest = function (options) {
            var clientResponse;
            var requestOptions = {
                method: options.method,
                url: "https://".concat(this.apiKey, ":").concat(this.apiPassword, "@").concat(this.domainName, "/admin/api/").concat(this.apiVersion, "/").concat(options.resource),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            if (options.payload) {
                requestOptions.body = JSON.stringify(options.payload);
            }
            try {
                clientResponse = https.request(requestOptions);
                if (clientResponse.headers['X-Shopify-Api-Version'] && clientResponse.headers['X-Shopify-Api-Version'] !== this.apiVersion) {
                    log.emergency('makeRequest - Update your API Version!', "Currently using ".concat(this.apiVersion, ", should be using ").concat(clientResponse.headers['X-Shopify-Api-Version']));
                }
                switch (+clientResponse.code) {
                    case 200: {
                        if (options.OK) {
                            options.OK(clientResponse);
                        }
                        else if (options.Deleted) {
                            options.Deleted(clientResponse);
                        }
                        break;
                    }
                    case 201: {
                        if (options.Created) {
                            options.Created(clientResponse);
                        }
                        break;
                    }
                    case 429: {
                        log.emergency('makeRequest - Too Many Requests', "X-Shopify-Shop-Api-Call-Limit: ".concat(clientResponse.headers['X-Shopify-Shop-Api-Call-Limit'], "  :  Retry-After: ").concat(clientResponse.headers['Retry-After']));
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                        break;
                    }
                    default: {
                        if (options.Failed) {
                            options.Failed(clientResponse);
                        }
                        break;
                    }
                }
            }
            catch (error) {
                if (options.retryCount <= 5) {
                    options.retryCount = options.retryCount + 1;
                    this.makeRequest(options);
                }
                else {
                    log.emergency('makeRequest - Error', error);
                    options.Failed(clientResponse);
                }
            }
        };
        return ShopifyAPI;
    }());
    exports.ShopifyAPI = ShopifyAPI;
});
