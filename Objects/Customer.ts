/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Address} from './Address';
import {CreateCustomerAddressContextFunction, GetContextCustomerAddressesFunction, GetCustomerAddressFunctionContext, UpdateCustomerAddressContextFunction} from '../Interfaces/ICustomerAddress';

export interface Customer {
    id: number;
    email: string;
    accepts_marketing: boolean;
    created_at?: string; // '2012-02-15T15:12:21-05:00'
    updated_at?: string; // '2012-02-15T15:12:21-05:00'
    first_name: string;
    last_name: string;
    orders_count: number;
    state: string;
    total_spent: string;
    last_order_id?: number;
    note?: string;
    verified_email: boolean;
    multipass_identifier?: string;
    tax_exempt: boolean;
    phone?: string;
    tags: string;
    last_order_name?: string;
    default_address: Address;

    getAddresses: GetContextCustomerAddressesFunction;
    getAddress: GetCustomerAddressFunctionContext;
    createAddress: CreateCustomerAddressContextFunction;
    updateAddress: UpdateCustomerAddressContextFunction;
}
