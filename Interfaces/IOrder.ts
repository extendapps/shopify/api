/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {FinancialStatus, FulfillmentStatus} from './ShopifyTypes';
import {BaseOptions, BaseSearchOptions} from './Base';
import {Order} from '../Objects/Order';

export interface OrderBaseOptions extends BaseOptions {
    id: number;
}

// tslint:disable-next-line:no-empty-interface
export type OrdersBaseOptions = BaseOptions;

export interface GetOrdersBaseOptions extends OrdersBaseOptions, BaseSearchOptions {
    order?: string;
    processed_at_min?: string; // Show orders imported at or after date (format: 2014-04-25T16:15:47-04:00).
    processed_at_max?: string; // Show orders imported at or before date (format: 2014-04-25T16:15:47-04:00).
    attribution_app_id?: string; // Show orders attributed to a certain app, specified by the app ID. Set as current to show orders for the app currently consuming the API.
    status?: 'open' | 'closed' | 'cancelled' | 'any'; // Filter orders by their status. (default: open)
    financial_status?: FinancialStatus; // Filter orders by their financial status. (default: any)
    fulfillment_status?: FulfillmentStatus; // Filter orders by their fulfillment status. (default: any)
}

export interface GetOrderOptions extends OrderBaseOptions, BaseSearchOptions {
    OK: GetOrderCallback;
}

export interface GetOrdersOptions extends GetOrdersBaseOptions {
    OK: GetOrdersCallback;
}

export interface GetOrdersCountOptions extends GetOrdersBaseOptions {
    OK: GetOrdersCountCallback;
}

export type GetOrderCallback = (order: Order) => void;

export interface GetOrderResponse {
    order: Order;
}

export type GetOrdersCallback = (orders: Order[]) => void;

export interface GetOrdersResponse {
    orders: Order[];
}

export type GetOrdersCountCallback = (count: number) => void;

export interface GetOrdersCountResponse {
    count: number;
}
