/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {RefundCallBack, RefundsCallBack} from './IRefund';
import {OrderBaseOptions} from './IOrder';
import {RefundLineItem} from '../Objects/RefundLineItem';

export interface OrderRefundOptions extends OrderBaseOptions {
    refundId: number;
}

export type GetContextOrderRefundFunction = (options: GetContextOrderRefund) => void;

export interface GetContextOrderRefund extends OrderBaseOptions {
    OK: RefundCallBack;
}

export interface GetOrderRefundOptions extends GetContextOrderRefund, OrderRefundOptions {}

export type GetContextOrderRefundsFunction = (options: GetOrderRefundsOptions) => void;

export interface GetContextOrderRefundsOptions extends OrderBaseOptions {
    since_id?: number; // Retrieve only transactions after the specified ID.
    fields?: string; // Show only certain fields, specifed by a comma-separated list of fields names.
    OK: RefundsCallBack;
}

// tslint:disable-next-line:no-empty-interface
export type GetOrderRefundsOptions = GetContextOrderRefundsOptions;

export interface CalculateOrderRefundOptions extends OrderBaseOptions {
    refund: {
        shipping: {
            full_refund?: boolean;
            amount?: number;
        };
        refund_line_items?: RefundLineItem[];
    };
    OK: RefundCallBack;
}
