/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {IOption} from './IOption';
import {IImage} from './IImage';
import {GetContextProductVariantsCountFunction, GetContextProductVariantsFunction} from '../Interfaces/IProductVariants';
import {GetContextProductImagesFunction} from '../Interfaces/IProductImages';
import {Variant} from './Variant';

export interface Product {
    id?: number;
    title: string;
    body_html: string;
    vendor: string;
    product_type: string;
    created_at?: string; // '2012-02-15T15:12:21-05:00'
    handle?: string;
    updated_at?: string; // '2012-02-15T15:12:21-05:00'
    published_at?: string; // '2012-02-15T15:12:21-05:00'
    template_suffix?: string;
    tags?: string;
    published_scope?: string;
    admin_graphql_api_id?: string;
    variants?: Variant[];
    options?: IOption[];
    images?: IImage[];
    image?: IImage;
    published?: boolean;
    getVariants?: GetContextProductVariantsFunction;
    getVariantsCount?: GetContextProductVariantsCountFunction;
    getImages?: GetContextProductImagesFunction;
}
