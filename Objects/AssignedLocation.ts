/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface AssignedLocation {
    address1?: string;
    address2?: string;
    city?: string;
    country_code: string;
    location_id: number;
    name: string;
    phone?: string;
    province?: string;
    zip?: string;
}
