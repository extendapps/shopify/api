/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface DiscountAllocation {
    amount: string;
    discount_application_index: number;
}
