/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {FulfillmentService} from '../Objects/FulfillmentService';

export type FulfillmentServiceCallBack = (fulfillmentService: FulfillmentService) => void;
export type FulfillmentServicesCallBack = (fulfillmentService: FulfillmentService[]) => void;
export type FulfillmentServicesDeleteCallBack = () => void;

export interface FulfillmentServicePayload {
    fulfillment_service: FulfillmentService;
}

export interface FulfillmentServicesPayload {
    fulfillment_services: FulfillmentService[];
}

export interface FulfillmentServiceBaseOptions extends BaseOptions {
    id: number;
}

export interface GetFulfillmentServiceContextOptions {
    OK: FulfillmentServiceCallBack;
}

export interface GetFulfillmentServicesContextOptions {
    OK: FulfillmentServicesCallBack;
}

export interface GetFulfillmentServiceOptions extends GetFulfillmentServiceContextOptions, FulfillmentServiceBaseOptions {}

export interface GetFulfillmentServicesOptions extends GetFulfillmentServicesContextOptions, FulfillmentServiceBaseOptions {}

export interface UpdateFulfillmentServiceContextOptions extends BaseOptions, FulfillmentServicePayload {
    OK: FulfillmentServiceCallBack;
}

export interface CreateFulfillmentServiceContextOptions extends BaseOptions, FulfillmentServicePayload {
    Created: FulfillmentServiceCallBack;
}

export interface DeleteFulfillmentServiceOptions extends FulfillmentServiceBaseOptions {
    Deleted: FulfillmentServicesDeleteCallBack;
}

export interface CreateFulfillmentServiceOptions extends BaseOptions, CreateFulfillmentServiceContextOptions {}

// tslint:disable-next-line:no-empty-interface
export type UpdateFulfillmentServiceOptions = UpdateFulfillmentServiceContextOptions;
