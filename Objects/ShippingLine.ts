/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {TaxLine} from './TaxLine';

export interface ShippingLine {
    id: number;
    title: string;
    price: string;
    code?: string;
    source: string;
    phone?: string;
    requested_fulfillment_service_id?: number;
    delivery_category?: string;
    carrier_identifier?: string;
    discounted_price: string;
    discount_allocations: string[];
    tax_lines: TaxLine[];
}
