/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {BaseOptions} from './Base';
import {Fulfillment} from '../Objects/Fulfillment';

export interface CreateFulfillmentOptions extends BaseOptions {
    payload: CreateFulfillmentPayload;
    Created: FulfillmentCallback;
}

export interface CreateFulfillmentPayload {
    line_items_by_fulfillment_order: line_items_by_fulfillment_order[];
    message?: string;
    notify_customer: boolean;
    origin_address?: {address1: string; address2?: string; city: string; country_code: string; province_code: string; zip: string};
    tracking_info?: {company: string; number: string; url?: string};
}

export interface line_items_by_fulfillment_order {
    fulfillment_order_id: number;

    fulfillment_order_line_items: {id: number; quantity: number}[];
}

export interface CompleteFulfillmentOptions extends BaseOptions {
    order_id: number;
    fulfillment_id: number;
    Created: FulfillmentCallback;
}

export type FulfillmentCallback = (fulfillment: Fulfillment) => void;

export interface FulfillmentResponse {
    fulfillment: Fulfillment;
}
