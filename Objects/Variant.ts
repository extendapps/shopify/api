/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface Variant {
    id?: number;
    product_id: number;
    title?: string;
    price: string;
    sku?: string;
    position?: number;
    inventory_policy?: string;
    compare_at_price?: string;
    fulfillment_service?: string;
    inventory_management?: string;
    option1: string;
    option2?: string;
    option3?: string;
    created_at?: string; // '2012-02-15T15:12:21-05:00'
    updated_at?: string; // '2012-02-15T15:12:21-05:00'
    taxable?: boolean;
    barcode?: string;
    grams?: number;
    image_id?: number;
    inventory_quantity?: number;
    weight?: number;
    weight_unit?: string;
    inventory_item_id?: number;
    old_inventory_quantity?: number;
    requires_shipping?: boolean;
    admin_graphql_api_id?: string;
}
