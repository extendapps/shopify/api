/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * KeyGen.js
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

import * as https from 'N/https';
import * as crypto from 'N/crypto';
import * as log from 'N/log';
import {CreateOrderTransactionOptions, GetOrderTransactionCountOptions, GetOrderTransactionOptions, GetOrderTransactionsOptions} from './Interfaces/IOrderTransaction';
import {TransactionCountPayload, TransactionPayload, TransactionsPayload} from './Interfaces/ITransaction';
import {CreateFulfillmentServiceOptions, DeleteFulfillmentServiceOptions, FulfillmentServicePayload, FulfillmentServicesPayload, GetFulfillmentServiceOptions, GetFulfillmentServicesOptions, UpdateFulfillmentServiceOptions} from './Interfaces/IFulfillmentService';
import {CreateCustomerAddressOptions, CustomerAddressPayload, GetCustomerAddressesOptions, GetCustomerAddressOptions, UpdateCustomerAddressOptions} from './Interfaces/ICustomerAddress';
import {AddressesPayload} from './Interfaces/IAddress';
import {CreateWebHookOptions, DeleteWebHookOptions, GetWebHookOptions, GetWebHooksOptions, UpdateWebHookOptions, ValidateWebHookOptions, WebHookPayload, WebHooksPayload} from './Interfaces/IWebHook';
import {GetShopPropertiesOptions, ShopPropertiesPayload} from './Interfaces/IShop';
import {CreateProductVariantOptions, DeleteProductVariantOptions, GetProductVariantOptions, GetProductVariantsCountOptions, GetProductVariantsOptions, UpdateProductVariantOptions} from './Interfaces/IProductVariants';
import {VariantPayload, VariantsCountPayload, VariantsPayload} from './Interfaces/IVariant';
import {GetProductImagesOptions} from './Interfaces/IProductImages';
import {IImagesPayload} from './Interfaces/IImage';
import {CreateProductOptions, DeleteProductOptions, GetProductOptions, GetProductsOptions, ProductPayload, ProductsPayload, UpdateProductOptions} from './Interfaces/IProduct';
import {CalculateOrderRefundOptions, GetOrderRefundOptions, GetOrderRefundsOptions} from './Interfaces/IOrderRefund';
import {RefundPayload, RefundsPayload} from './Interfaces/IRefund';
import {GetOrderOptions, GetOrderResponse, GetOrdersCountOptions, GetOrdersCountResponse, GetOrdersOptions, GetOrdersResponse} from './Interfaces/IOrder';
import {Order} from './Objects/Order';
import {Product} from './Objects/Product';
import {Customer} from './Objects/Customer';
import {GetLocationsOptions, GetLocationsResponse} from './Objects/Location';
import {AdjustInventoryLevelsOptions, ConnectInventoryLevelOptions, DeleteInventoryLevelOptions, GetInventoryLevelResponse, GetInventoryLevelsOptions, InventoryLevelResponse, SetInventoryLevelOptions} from './Interfaces/IInventoryLevel';
import {GetFulfillmentOrdersOptions, GetFulfillmentOrdersResponse} from './Interfaces/IFulfillmentOrder';
import {CompleteFulfillmentOptions, CreateFulfillmentOptions, FulfillmentResponse} from './Interfaces/IFulfillment';
import {UpdateInventoryItemOptions} from './Interfaces/IInventoryItem';
import {InventoryItem} from './Objects/InventoryItem';

// noinspection JSUnusedGlobalSymbols,DuplicatedCode
export class ShopifyAPI {
    constructor(
        private readonly apiKey: string,
        private readonly apiPassword: string,
        private readonly domainName: string,
        private readonly apiSecretGUID: string,
        private readonly apiVersion: string = '2023-01',
    ) {}

    public static decorateOrder(order: Order, shopfy: ShopifyAPI) {
        order.getTransaction = (options: GetOrderTransactionOptions): void => {
            options.id = order.id;
            shopfy.getOrderTransaction(options);
        };
        order.getTransactions = (options: GetOrderTransactionsOptions): void => {
            options.id = order.id;
            shopfy.getOrderTransactions(options);
        };
        order.getTransactionCount = (options: GetOrderTransactionCountOptions): void => {
            options.id = order.id;
            shopfy.getOrderTransactionCount(options);
        };
        order.createTransaction = (options: CreateOrderTransactionOptions): void => {
            options.id = order.id;
            shopfy.createOrderTransaction(options);
        };
        order.getRefund = (options: GetOrderRefundOptions): void => {
            options.id = order.id;
            shopfy.getOrderRefund(options);
        };
        order.getRefunds = (options: GetOrderRefundsOptions): void => {
            options.id = order.id;
            shopfy.getOrderRefunds(options);
        };
    }

    public static decorateCustomer(customer: Customer, shopfy: ShopifyAPI) {
        customer.getAddress = (options: GetCustomerAddressOptions): void => {
            options.customerId = customer.id;
            shopfy.getCustomerAddress(options);
        };
        customer.getAddresses = (options: GetCustomerAddressesOptions): void => {
            options.customerId = customer.id;
            shopfy.getCustomerAddresses(options);
        };

        customer.createAddress = (options: CreateCustomerAddressOptions): void => {
            options.customerId = customer.id;
            shopfy.createCustomerAddress(options);
        };

        customer.updateAddress = (options: UpdateCustomerAddressOptions): void => {
            options.customerId = customer.id;
            shopfy.updateCustomerAddress(options);
        };
    }

    public static decorateProduct(product: Product, shopfy: ShopifyAPI) {
        product.getVariants = (options: GetProductVariantsOptions): void => {
            options.product_id = product.id;
            shopfy.getProductVariants(options);
        };

        product.getImages = (options: GetProductImagesOptions): void => {
            options.product_id = product.id;
            shopfy.getProductImages(options);
        };

        product.getVariantsCount = (options: GetProductVariantsCountOptions): void => {
            options.product_id = product.id;
            shopfy.getProductVariantsCount(options);
        };
    }

    public getFulfillmentOrders(options: GetFulfillmentOrdersOptions) {
        this.makeGETRequest({
            resource: `orders/${options.order_id}/fulfillment_orders.json`,
            OK: (clientResponse) => {
                const getFulfillmentOrdersResponse: GetFulfillmentOrdersResponse = JSON.parse(clientResponse.body);

                options.OK(getFulfillmentOrdersResponse.fulfillment_orders);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getFulfillmentOrders', clientResponse);
                }
            },
        });
    }

    public createFulfillment(options: CreateFulfillmentOptions) {
        this.makePOSTRequest({
            resource: `fulfillments.json`,
            payload: {
                fulfillment: options.payload,
            },
            Created: (clientResponse) => {
                const fulfillmentResponse: FulfillmentResponse = JSON.parse(clientResponse.body);
                options.Created(fulfillmentResponse.fulfillment);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createFulfillment', clientResponse);
                }
            },
        });
    }

    public completeFulfillment(options: CompleteFulfillmentOptions) {
        this.makePOSTRequest({
            resource: `orders/${options.order_id}/fulfillments/${options.fulfillment_id}/complete.json`,
            payload: {},
            Created: (clientResponse) => {
                const fulfillmentResponse: FulfillmentResponse = JSON.parse(clientResponse.body);
                options.Created(fulfillmentResponse.fulfillment);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('completeFulfillment', clientResponse);
                }
            },
        });
    }

    public getLocations(options: GetLocationsOptions) {
        this.makeGETRequest({
            resource: `locations.json`,
            OK: (clientResponse) => {
                const getLocationsResponse: GetLocationsResponse = JSON.parse(clientResponse.body);

                options.OK(getLocationsResponse.locations);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getLocations', clientResponse);
                }
            },
        });
    }

    public getOrders(options: GetOrdersOptions) {
        let getOrdersURL = `orders.json`;
        const filters: string[] = [];

        if (options.ids) {
            filters.push(`ids=${options.ids.join(',')}`);
        }

        if (options.limit) {
            filters.push(`limit=${options.limit}`);
        }

        if (options.since_id) {
            filters.push(`since_id=${options.since_id}`);
        }

        if (options.created_at_min) {
            filters.push(`created_at_min=${options.created_at_min}`);
        }

        if (options.created_at_max) {
            filters.push(`created_at_max=${options.created_at_max}`);
        }

        if (options.updated_at_min) {
            filters.push(`updated_at_min=${options.updated_at_min}`);
        }

        if (options.updated_at_max) {
            filters.push(`updated_at_max=${options.updated_at_max}`);
        }

        if (options.processed_at_min) {
            filters.push(`processed_at_min=${options.processed_at_min}`);
        }

        if (options.processed_at_max) {
            filters.push(`processed_at_max=${options.processed_at_max}`);
        }

        if (options.attribution_app_id) {
            filters.push(`attribution_app_id=${options.attribution_app_id}`);
        }

        if (options.status) {
            filters.push(`status=${options.status}`);
        }

        if (options.financial_status) {
            filters.push(`financial_status=${options.financial_status}`);
        }

        if (options.fulfillment_status) {
            filters.push(`fulfillment_status=${options.fulfillment_status}`);
        }

        if (options.fields) {
            filters.push(`fields=${options.fields.join(',')}`);
        }

        if (filters.length > 0) {
            getOrdersURL = `${getOrdersURL}?${filters.join('&')}`;
        }

        if (options.order) {
            getOrdersURL = `${getOrdersURL}&order=${options.order}`;
        }

        this.makeGETRequest({
            resource: getOrdersURL,
            OK: (clientResponse) => {
                const getOrdersResponse: GetOrdersResponse = JSON.parse(clientResponse.body);

                for (const order of getOrdersResponse.orders) {
                    ShopifyAPI.decorateOrder(order, this);
                }

                options.OK(getOrdersResponse.orders);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrders', clientResponse);
                }
            },
        });
    }

    public getOrdersCount(options: GetOrdersCountOptions) {
        let getOrdersCountURL = `orders/count.json`;
        const filters: string[] = [];

        if (options.ids) {
            filters.push(`ids=${options.ids.join(',')}`);
        }

        if (options.limit) {
            filters.push(`limit=${options.limit}`);
        }

        if (options.since_id) {
            filters.push(`since_id=${options.since_id}`);
        }

        if (options.created_at_min) {
            filters.push(`created_at_min=${options.created_at_min}`);
        }

        if (options.created_at_max) {
            filters.push(`created_at_max=${options.created_at_max}`);
        }

        if (options.updated_at_min) {
            filters.push(`updated_at_min=${options.updated_at_min}`);
        }

        if (options.updated_at_max) {
            filters.push(`updated_at_max=${options.updated_at_max}`);
        }

        if (options.processed_at_min) {
            filters.push(`processed_at_min=${options.processed_at_min}`);
        }

        if (options.processed_at_max) {
            filters.push(`processed_at_max=${options.processed_at_max}`);
        }

        if (options.attribution_app_id) {
            filters.push(`attribution_app_id=${options.attribution_app_id}`);
        }

        if (options.status) {
            filters.push(`status=${options.status}`);
        }

        if (options.financial_status) {
            filters.push(`financial_status=${options.financial_status}`);
        }

        if (options.fulfillment_status) {
            filters.push(`fulfillment_status=${options.fulfillment_status}`);
        }

        if (options.fields) {
            filters.push(`fields=${options.fields.join(',')}`);
        }

        if (filters.length > 0) {
            getOrdersCountURL = `${getOrdersCountURL}?${filters.join('&')}`;
        }

        this.makeGETRequest({
            resource: getOrdersCountURL,
            OK: (clientResponse) => {
                const getOrdersCountResponse: GetOrdersCountResponse = JSON.parse(clientResponse.body);

                options.OK(getOrdersCountResponse.count);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getORdersCount', clientResponse);
                }
            },
        });
    }

    public getOrder(options: GetOrderOptions) {
        this.makeGETRequest({
            resource: `orders/${options.id}.json`,
            OK: (clientResponse) => {
                const getOrderResponse: GetOrderResponse = JSON.parse(clientResponse.body);
                options.OK(getOrderResponse.order);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrder', clientResponse);
                }
            },
        });
    }

    public getOrderRefunds(options: GetOrderRefundsOptions) {
        this.makeGETRequest({
            resource: `orders/${options.id}/refunds.json`,
            OK: (clientResponse) => {
                const getOrderRefundsResponse: RefundsPayload = JSON.parse(clientResponse.body);
                options.OK(getOrderRefundsResponse.refunds);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrderRefunds', clientResponse);
                }
            },
        });
    }

    public getOrderRefund(options: GetOrderRefundOptions) {
        this.makeGETRequest({
            resource: `orders/${options.id}/refunds/${options.refundId}.json`,
            OK: (clientResponse) => {
                const getOrderRefundResponse: RefundPayload = JSON.parse(clientResponse.body);
                options.OK(getOrderRefundResponse.refund);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrderRefund', clientResponse);
                }
            },
        });
    }

    public calculateOrderRefund(options: CalculateOrderRefundOptions) {
        this.makeRequest({
            method: https.Method.POST,
            resource: `orders/${options.id}/refunds/calculate.json`,
            payload: {
                refund: options.refund,
            },
            OK: (clientResponse) => {
                const calculateOrderRefundResponse: RefundPayload = JSON.parse(clientResponse.body);
                options.OK(calculateOrderRefundResponse.refund);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('calculateOrderRefund', clientResponse);
                }
            },
            retryCount: 0,
        });
    }

    public getOrderTransactions(options: GetOrderTransactionsOptions) {
        let getOrderTransactionsURL = `orders/${options.id}/transactions.json`;
        const extraParams: string[] = [];
        if (options.since_id) {
            extraParams.push(`since_id=${options.since_id}`);
        }
        if (options.fields) {
            extraParams.push(`fields=${options.fields}`);
        }
        if (extraParams.length > 0) {
            getOrderTransactionsURL = `${getOrderTransactionsURL}?${extraParams.join('&')}`;
        }
        this.makeGETRequest({
            resource: getOrderTransactionsURL,
            OK: (clientResponse) => {
                const orderTransactionsResponse: TransactionsPayload = JSON.parse(clientResponse.body);
                options.OK(orderTransactionsResponse.transactions);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrderTransactions', clientResponse);
                }
            },
        });
    }

    public getOrderTransactionCount(options: GetOrderTransactionCountOptions) {
        this.makeGETRequest({
            resource: `orders/${options.id}/transactions/count.json`,
            OK: (clientResponse) => {
                const getOrderTransactionCountResponse: TransactionCountPayload = JSON.parse(clientResponse.body);
                options.OK(getOrderTransactionCountResponse.count);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrderTransactionCount', clientResponse);
                }
            },
        });
    }

    public getOrderTransaction(options: GetOrderTransactionOptions) {
        this.makeGETRequest({
            resource: `orders/${options.id}/transactions/${options.transactionId}.json`,
            OK: (clientResponse) => {
                const getOrderTransactionResponse: TransactionPayload = JSON.parse(clientResponse.body);
                options.OK(getOrderTransactionResponse.transaction);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getOrderTransaction', clientResponse);
                }
            },
        });
    }

    public createOrderTransaction(options: CreateOrderTransactionOptions) {
        this.makePOSTRequest({
            resource: `orders/${options.id}/transactions.json`,
            payload: {
                transaction: options.transaction,
            },
            Created: (clientResponse) => {
                const createOrderTransactionResponse: TransactionPayload = JSON.parse(clientResponse.body);
                options.Created(createOrderTransactionResponse.transaction);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createOrderTransaction', clientResponse);
                }
            },
        });
    }

    public updateProduct(options: UpdateProductOptions) {
        this.makePUTRequest({
            resource: `products/${options.product.id}.json`,
            payload: {
                product: options.product,
            },
            OK: (clientResponse) => {
                const productResponse: ProductPayload = JSON.parse(clientResponse.body);
                ShopifyAPI.decorateProduct(productResponse.product, this);
                options.OK(productResponse.product);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateProduct', clientResponse);
                }
            },
        });
    }

    public createProduct(options: CreateProductOptions) {
        this.makePOSTRequest({
            resource: `products.json`,
            payload: {
                product: options.product,
            },
            Created: (clientResponse) => {
                const productResponse: ProductPayload = JSON.parse(clientResponse.body);
                ShopifyAPI.decorateProduct(productResponse.product, this);
                options.Created(productResponse.product);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createProduct', clientResponse);
                }
            },
        });
    }

    public getProducts(options: GetProductsOptions) {
        let getProductsURL = `products.json`;
        const filters: string[] = [];

        if (options.ids) {
            filters.push(`ids=${options.ids.join(',')}`);
        }

        if (options.limit) {
            filters.push(`limit=${options.limit}`);
        }

        if (options.since_id) {
            filters.push(`since_id=${options.since_id}`);
        }

        if (options.created_at_min) {
            filters.push(`created_at_min=${options.created_at_min}`);
        }

        if (options.created_at_max) {
            filters.push(`created_at_max=${options.created_at_max}`);
        }

        if (options.updated_at_min) {
            filters.push(`updated_at_min=${options.updated_at_min}`);
        }

        if (options.updated_at_max) {
            filters.push(`updated_at_max=${options.updated_at_max}`);
        }

        if (options.title) {
            filters.push(`title=${options.title}`);
        }

        if (options.vendor) {
            filters.push(`vendor=${options.vendor}`);
        }

        if (options.handle) {
            filters.push(`handle=${options.handle}`);
        }

        if (options.product_type) {
            filters.push(`product_type=${options.product_type}`);
        }

        if (options.collection_id) {
            filters.push(`collection_id=${options.collection_id}`);
        }

        if (options.fields) {
            filters.push(`fields=${options.fields.join(',')}`);
        }

        if (filters.length > 0) {
            getProductsURL = `${getProductsURL}?${filters.join('&')}`;
        }

        this.makeGETRequest({
            resource: getProductsURL,
            OK: (clientResponse) => {
                const productsResponse: ProductsPayload = JSON.parse(clientResponse.body);

                for (const product of productsResponse.products) {
                    ShopifyAPI.decorateProduct(product, this);
                }

                options.OK(productsResponse.products);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProducts', clientResponse);
                }
            },
        });
    }

    public getProduct(options: GetProductOptions) {
        this.makeGETRequest({
            resource: `products/${options.product_id}.json`,
            OK: (clientResponse) => {
                const productResponse: ProductPayload = JSON.parse(clientResponse.body);
                ShopifyAPI.decorateProduct(productResponse.product, this);
                options.OK(productResponse.product);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProduct', clientResponse);
                }
            },
        });
    }

    public deleteProduct(options: DeleteProductOptions) {
        this.makeDELETERequest({
            resource: `products/${options.product_id}.json`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('deleteProduct', clientResponse);
                }
            },
        });
    }

    public getProductImages(options: GetProductImagesOptions) {
        this.makeGETRequest({
            resource: `products/${options.product_id}/images.json`,
            OK: (clientResponse) => {
                const getProductImagesResponse: IImagesPayload = JSON.parse(clientResponse.body);
                options.OK(getProductImagesResponse.images);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProductImages', clientResponse);
                }
            },
        });
    }

    public getProductVariants(options: GetProductVariantsOptions) {
        this.makeGETRequest({
            resource: `products/${options.product_id}/variants.json`,
            OK: (clientResponse) => {
                const productVariantsResponse: VariantsPayload = JSON.parse(clientResponse.body);
                options.OK(productVariantsResponse.variants);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProductVariants', clientResponse);
                }
            },
        });
    }

    public getProductVariantsCount(options: GetProductVariantsCountOptions) {
        this.makeGETRequest({
            resource: `products/${options.product_id}/variants/count.json`,
            OK: (clientResponse) => {
                const productVariantsCountResponse: VariantsCountPayload = JSON.parse(clientResponse.body);
                options.OK(productVariantsCountResponse.count);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProductVariants', clientResponse);
                }
            },
        });
    }

    public getProductVariant(options: GetProductVariantOptions) {
        this.makeGETRequest({
            resource: `variants/${options.variant_id}.json`,
            OK: (clientResponse) => {
                const getProductVariantResponse: VariantPayload = JSON.parse(clientResponse.body);
                options.OK(getProductVariantResponse.variant);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getProductVariant', clientResponse);
                }
            },
        });
    }

    public updateProductVariant(options: UpdateProductVariantOptions) {
        this.makePUTRequest({
            resource: `variants/${options.variant.id}.json`,
            payload: {
                variant: options.variant,
            },
            OK: (clientResponse) => {
                const updateProductVariantResponse: VariantPayload = JSON.parse(clientResponse.body);
                options.OK(updateProductVariantResponse.variant);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateProductVariant', clientResponse);
                }
            },
        });
    }

    public createProductVariant(options: CreateProductVariantOptions) {
        this.makePOSTRequest({
            resource: `products/${options.variant.product_id}/variants.json`,
            payload: {
                variant: options.variant,
            },
            Created: (clientResponse) => {
                const createProductVariantResponse: VariantPayload = JSON.parse(clientResponse.body);
                options.Created(createProductVariantResponse.variant);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createProductVariant', clientResponse);
                }
            },
        });
    }

    public deleteProductVariant(options: DeleteProductVariantOptions) {
        this.makeDELETERequest({
            resource: `products/${options.product_id}/variants/${options.variant_id}`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('deleteProductVariant', clientResponse);
                }
            },
        });
    }

    public getShopProperties(options: GetShopPropertiesOptions) {
        this.makeGETRequest({
            resource: `shop.json`,
            OK: (clientResponse) => {
                const setShopPropertiesResponse: ShopPropertiesPayload = JSON.parse(clientResponse.body);
                options.OK(setShopPropertiesResponse.shop);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getShopProperties', clientResponse);
                }
            },
        });
    }

    public validateWebHook(options: ValidateWebHookOptions) {
        if (options.hmacHeader === null) {
            options.Validated();
        } else {
            const hmacSHA256 = crypto.createHmac({
                algorithm: crypto.HashAlg.SHA256,
                key: crypto.createSecretKey({
                    guid: this.apiSecretGUID,
                    encoding: crypto.Encoding.UTF_8,
                }),
            });
            hmacSHA256.update({
                input: typeof options.webHookData === 'object' ? JSON.stringify(options.webHookData) : options.webHookData,
                inputEncoding: crypto.Encoding.UTF_8,
            });

            const calculatedHMAC = hmacSHA256.digest({
                outputEncoding: crypto.Encoding.BASE_64,
            });

            // Setting the hmacHeader to null indicates we want to skip validation
            if (options.hmacHeader === calculatedHMAC) {
                options.Validated();
            } else {
                options.Failed();
            }
        }
    }

    public getWebHook(options: GetWebHookOptions) {
        this.makeGETRequest({
            resource: `webhooks/${options.webhookId}.json`,
            OK: (clientResponse) => {
                const getWebHookResponse: WebHookPayload = JSON.parse(clientResponse.body);
                options.OK(getWebHookResponse.webhook);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getWebHook', clientResponse);
                }
            },
        });
    }

    public getWebHooks(options: GetWebHooksOptions) {
        this.makeGETRequest({
            resource: `webhooks.json`,
            OK: (clientResponse) => {
                const getWebHooksResponse: WebHooksPayload = JSON.parse(clientResponse.body);
                options.OK(getWebHooksResponse.webhooks);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getWebHooks', clientResponse);
                }
            },
        });
    }

    public createWebHook(options: CreateWebHookOptions) {
        this.makePOSTRequest({
            resource: `webhooks.json`,
            payload: {
                webhook: options.webhook,
            },
            Created: (clientResponse) => {
                const createtWebHookResponse: WebHookPayload = JSON.parse(clientResponse.body);
                options.Created(createtWebHookResponse.webhook);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createWebHook', clientResponse);
                }
            },
        });
    }

    public updateWebHook(options: UpdateWebHookOptions) {
        this.makePUTRequest({
            resource: `webhooks/${options.webhook.id}.json`,
            payload: {
                webhook: options.webhook,
            },
            OK: (clientResponse) => {
                const updatetWebHookResponse: WebHookPayload = JSON.parse(clientResponse.body);
                options.OK(updatetWebHookResponse.webhook);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateWebHook', clientResponse);
                }
            },
        });
    }

    public deleteWebHook(options: DeleteWebHookOptions) {
        this.makeDELETERequest({
            resource: `webhooks/${options.webhook.id}.json`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('deleteWebHook', clientResponse);
                }
            },
        });
    }

    public getCustomerAddresses(options: GetCustomerAddressesOptions) {
        this.makeGETRequest({
            resource: `customers/${options.customerId}/addresses.json`,
            OK: (clientResponse) => {
                const getCustomerAddressesResponse: AddressesPayload = JSON.parse(clientResponse.body);
                options.OK(getCustomerAddressesResponse.addresses);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getCustomerAddresses', clientResponse);
                }
            },
        });
    }

    public getCustomerAddress(options: GetCustomerAddressOptions) {
        this.makeGETRequest({
            resource: `customers/${options.customerId}/addresses/${options.addressId}.json`,
            OK: (clientResponse) => {
                const getCustomerAddressResponse: CustomerAddressPayload = JSON.parse(clientResponse.body);
                options.OK(getCustomerAddressResponse.customer_address);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getCustomerAddress', clientResponse);
                }
            },
        });
    }

    public createCustomerAddress(options: CreateCustomerAddressOptions) {
        this.makePOSTRequest({
            resource: `customers/${options.customerId}/addresses.json`,
            payload: {
                address: options.address,
            },
            Created: (clientResponse) => {
                const customerAddressResponse: CustomerAddressPayload = JSON.parse(clientResponse.body);
                options.Created(customerAddressResponse.customer_address);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createCustomerAddress', clientResponse);
                }
            },
        });
    }

    public updateCustomerAddress(options: UpdateCustomerAddressOptions) {
        this.makePUTRequest({
            resource: `customers/${options.customerId}/addresses/${options.address.id}.json`,
            payload: {
                address: options.address,
            },
            OK: (clientResponse) => {
                const customerAddressResponse: CustomerAddressPayload = JSON.parse(clientResponse.body);
                options.OK(customerAddressResponse.customer_address);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateCustomerAddress', clientResponse);
                }
            },
        });
    }

    public getFulfillmentServices(options: GetFulfillmentServicesOptions) {
        this.makeGETRequest({
            resource: `fulfillment_services.json`,
            OK: (clientResponse) => {
                const getFulfillmentServicesResponse: FulfillmentServicesPayload = JSON.parse(clientResponse.body);
                options.OK(getFulfillmentServicesResponse.fulfillment_services);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getFulfillmentServices', clientResponse);
                }
            },
        });
    }

    public createFulfillmentService(options: CreateFulfillmentServiceOptions) {
        this.makePOSTRequest({
            resource: `fulfillment_services.json`,
            payload: {
                fulfillment_service: options.fulfillment_service,
            },
            Created: (clientResponse) => {
                const createFulfillmentServiceResponse: FulfillmentServicePayload = JSON.parse(clientResponse.body);
                options.Created(createFulfillmentServiceResponse.fulfillment_service);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('createFulfillmentService', clientResponse);
                }
            },
        });
    }

    public getFulfillmentService(options: GetFulfillmentServiceOptions) {
        this.makeGETRequest({
            resource: `fulfillment_services/${options.id}.json`,
            OK: (clientResponse) => {
                const getFulfillmentServiceResponse: FulfillmentServicePayload = JSON.parse(clientResponse.body);
                options.OK(getFulfillmentServiceResponse.fulfillment_service);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getFulfillmentService', clientResponse);
                }
            },
        });
    }

    public updateFulfillmentService(options: UpdateFulfillmentServiceOptions) {
        this.makePUTRequest({
            resource: `fulfillment_services/${options.fulfillment_service.id}.json`,
            payload: {
                fulfillment_service: options.fulfillment_service,
            },
            OK: (clientResponse) => {
                const updateFulfillmentServiceResponse: FulfillmentServicePayload = JSON.parse(clientResponse.body);
                options.OK(updateFulfillmentServiceResponse.fulfillment_service);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateFulfillmentService', clientResponse);
                }
            },
        });
    }

    public updateInventoryItem(options: UpdateInventoryItemOptions) {
        this.makePUTRequest({
            resource: `inventory_items/${options.inventory_item.id}.json`,
            payload: {
                inventory_item: options.inventory_item,
            },
            OK: (clientResponse) => {
                const updateResponse: {inventory_item: InventoryItem} = JSON.parse(clientResponse.body);
                options.OK(updateResponse.inventory_item);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('updateFulfillmentService', clientResponse);
                }
            },
        });
    }

    public deleteFulfillmentService(options: DeleteFulfillmentServiceOptions) {
        this.makeDELETERequest({
            resource: `fulfillment_services/${options.id}.json`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('deleteFulfillmentService', clientResponse);
                }
            },
        });
    }

    public getInventoryLevels(options: GetInventoryLevelsOptions) {
        let getInventoryLevelsURL = `inventory_levels.json`;
        const filters: string[] = [];

        if (options.inventory_item_ids) {
            filters.push(`inventory_item_ids=${options.inventory_item_ids.join(',')}`);
        }

        if (options.location_ids) {
            filters.push(`location_ids=${options.location_ids.join(',')}`);
        }

        if (options.limit) {
            filters.push(`limit=${options.limit}`);
        }

        if (options.updated_at_min) {
            filters.push(`updated_at_min=${options.updated_at_min}`);
        }

        if (filters.length > 0) {
            getInventoryLevelsURL = `${getInventoryLevelsURL}?${filters.join('&')}`;
        }

        this.makeGETRequest({
            resource: getInventoryLevelsURL,
            OK: (clientResponse) => {
                const getInventoryLevelResponse: GetInventoryLevelResponse = JSON.parse(clientResponse.body);

                options.OK(getInventoryLevelResponse.inventory_levels);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('getInventoryLevels', clientResponse);
                }
            },
        });
    }

    public adjustInventoryLevel(options: AdjustInventoryLevelsOptions) {
        this.makePOSTRequest({
            resource: `inventory_levels/adjust.json`,
            payload: {
                location_id: options.location_id,
                inventory_item_id: options.inventory_item_id,
                available_adjustment: options.available_adjustment,
            },
            OK: (clientResponse) => {
                const inventoryLevelResponse: InventoryLevelResponse = JSON.parse(clientResponse.body);
                options.OK(inventoryLevelResponse.inventory_level);
            },
            Created: (clientResponse) => {
                const inventoryLevelResponse: InventoryLevelResponse = JSON.parse(clientResponse.body);
                options.OK(inventoryLevelResponse.inventory_level);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('adjustInventoryLevel', clientResponse);
                }
            },
        });
    }

    public deleteInventoryLevel(options: DeleteInventoryLevelOptions) {
        this.makeDELETERequest({
            resource: `inventory_levels.json?inventory_item_id=${options.inventory_item_id}&location_id=${options.location_id}`,
            Deleted: () => {
                options.Deleted();
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('deleteInventoryLevel', clientResponse);
                }
            },
        });
    }

    public connectInventoryLevel(options: ConnectInventoryLevelOptions) {
        this.makePOSTRequest({
            resource: `inventory_levels/connect.json`,
            payload: {
                location_id: options.location_id,
                inventory_item_id: options.inventory_item_id,
                relocate_if_necessary: options.relocate_if_necessary,
            },
            Created: (clientResponse) => {
                const inventoryLevelResponse: InventoryLevelResponse = JSON.parse(clientResponse.body);
                options.Created(inventoryLevelResponse.inventory_level);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('connectInventoryLevel', clientResponse);
                }
            },
        });
    }

    public setInventoryLevel(options: SetInventoryLevelOptions) {
        this.makePOSTRequest({
            resource: `inventory_levels/set.json`,
            payload: {
                location_id: options.location_id,
                inventory_item_id: options.inventory_item_id,
                available: options.available,
                disconnect_if_necessary: options.disconnect_if_necessary,
            },
            OK: (clientResponse) => {
                const inventoryLevelResponse: InventoryLevelResponse = JSON.parse(clientResponse.body);
                options.OK(inventoryLevelResponse.inventory_level);
            },
            Created: (clientResponse) => {
                const inventoryLevelResponse: InventoryLevelResponse = JSON.parse(clientResponse.body);
                options.OK(inventoryLevelResponse.inventory_level);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                } else {
                    log.error('setInventoryLevel', clientResponse);
                }
            },
        });
    }

    private makeDELETERequest(options: MakeDELETERequestOptions) {
        this.makeRequest({
            method: https.Method.DELETE,
            resource: options.resource,
            Deleted: (clientResponse) => {
                options.Deleted(clientResponse);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            },
            retryCount: 0,
        });
    }

    private makeGETRequest(options: MakeGETRequestOptions) {
        this.makeRequest({
            method: https.Method.GET,
            resource: options.resource,
            OK: (clientResponse) => {
                options.OK(clientResponse);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            },
            retryCount: 0,
        });
    }

    private makePOSTRequest(options: MakePOSTRequestOptions) {
        this.makeRequest({
            method: https.Method.POST,
            resource: options.resource,
            payload: options.payload,
            OK: (clientResponse) => {
                if (options.OK) {
                    options.OK(clientResponse);
                }
            },
            Created: (clientResponse) => {
                options.Created(clientResponse);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            },
            retryCount: 0,
        });
    }

    private makePUTRequest(options: MakePUTRequestOptions) {
        this.makeRequest({
            method: https.Method.PUT,
            resource: options.resource,
            payload: options.payload,
            OK: (clientResponse) => {
                options.OK(clientResponse);
            },
            Failed: (clientResponse) => {
                if (options.Failed) {
                    options.Failed(clientResponse);
                }
            },
            retryCount: 0,
        });
    }

    private makeRequest(options: MakeRequestOptions) {
        let clientResponse: https.ClientResponse;
        const requestOptions: https.RequestOptions = {
            method: options.method,
            url: `https://${this.apiKey}:${this.apiPassword}@${this.domainName}/admin/api/${this.apiVersion}/${options.resource}`,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        if (options.payload) {
            requestOptions.body = JSON.stringify(options.payload);
        }

        try {
            clientResponse = https.request(requestOptions);

            if (clientResponse.headers['X-Shopify-Api-Version'] && clientResponse.headers['X-Shopify-Api-Version'] !== this.apiVersion) {
                log.emergency('makeRequest - Update your API Version!', `Currently using ${this.apiVersion}, should be using ${clientResponse.headers['X-Shopify-Api-Version']}`);
            }

            switch (+clientResponse.code) {
                case 200: {
                    if (options.OK) {
                        options.OK(clientResponse);
                    } else if (options.Deleted) {
                        options.Deleted(clientResponse);
                    }
                    break;
                }

                case 201: {
                    if (options.Created) {
                        options.Created(clientResponse);
                    }
                    break;
                }

                case 429: {
                    log.emergency('makeRequest - Too Many Requests', `X-Shopify-Shop-Api-Call-Limit: ${clientResponse.headers['X-Shopify-Shop-Api-Call-Limit']}  :  Retry-After: ${clientResponse.headers['Retry-After']}`);
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    break;
                }

                default: {
                    if (options.Failed) {
                        options.Failed(clientResponse);
                    }
                    break;
                }
            }
        } catch (error) {
            if (options.retryCount <= 5) {
                options.retryCount = options.retryCount + 1;
                this.makeRequest(options);
            } else {
                log.emergency('makeRequest - Error', error);
                options.Failed(clientResponse);
            }
        }
    }
}

interface MakeGETRequestOptions {
    resource: string;
    OK: ShopifyCallBack;
    Failed: ShopifyCallBack;
}

interface MakePOSTRequestOptions {
    resource: string;
    payload: unknown;
    Created: ShopifyCallBack;
    OK?: ShopifyCallBack;
    Failed: ShopifyCallBack;
}

interface MakeDELETERequestOptions {
    resource: string;
    Failed: ShopifyCallBack;

    Deleted: ShopifyCallBack;
}

interface MakePUTRequestOptions {
    resource: string;
    payload: unknown;
    OK: ShopifyCallBack;
    Failed: ShopifyCallBack;
}

interface MakeRequestOptions {
    method: https.Method;
    resource: string;
    payload?: unknown;
    OK?: ShopifyCallBack;
    Deleted?: ShopifyCallBack;
    Created?: ShopifyCallBack;
    Failed?: ShopifyCallBack;
    retryCount: number;
}

export type ShopifyCallBack = (clientResponse: https.ClientResponse) => void;
