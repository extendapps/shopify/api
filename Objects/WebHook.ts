/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {WebHookTopic} from '../Interfaces/ShopifyTypes';

export interface WebHook {
    address: string;
    created_at?: string; // '2012-02-15T15:12:21-05:00'
    fields?: string[];
    format: 'json' | 'xml';
    id?: number;
    metafield_namespaces?: string[];
    topic: WebHookTopic;
    updated_at?: string; // '2012-02-15T15:12:21-05:00'
}
