/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Transaction} from '../Objects/Transaction';

export type TransactionCallBack = (transaction: Transaction) => void;
export type TransactionsCallBack = (transactions: Transaction[]) => void;
export type TransactionDeleteCallBack = () => void;

export interface TransactionPayload {
    transaction: Transaction;
}

export interface TransactionsPayload {
    transactions: Transaction[];
}

export interface TransactionCountPayload {
    count: number;
}
