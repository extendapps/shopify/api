/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface FulfillmentService {
    id?: number;
    name: string;
    handle?: string;
    email?: string;
    include_pending_stock?: boolean;
    requires_shipping_method: boolean;
    service_name?: string;
    inventory_management: boolean;
    tracking_support: boolean;
    provider_id?: string;
    location_id?: number;
    format: 'json' | 'xml';
    callback_url?: string;
}
