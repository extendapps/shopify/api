/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {FulfillmentOrder} from '../Objects/FulfillmentOrder';

export interface GetFulfillmentOrdersOptions extends BaseOptions {
    order_id: number;
    OK: GetFulfillmentOrdersCallback;
}

export type GetFulfillmentOrdersCallback = (fulfillmentOrders: FulfillmentOrder[]) => void;

export interface GetFulfillmentOrdersResponse {
    fulfillment_orders: FulfillmentOrder[];
}
