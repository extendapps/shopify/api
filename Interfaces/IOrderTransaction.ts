/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {TransactionCallBack, TransactionDeleteCallBack, TransactionPayload, TransactionsCallBack} from './ITransaction';
import {BaseOptions} from './Base';
import {OrderBaseOptions} from './IOrder';
import {Transaction} from '../Objects/Transaction';

export interface OrderTransactionBaseOptions extends OrderBaseOptions {
    transactionId: number;
}

export interface GetOrderTransactionContextOptions {
    OK: TransactionCallBack;
}

export interface GetOrderTransactionsContextOptions {
    OK: TransactionsCallBack;
}

export type GetContextOrderTransactionFunction = (options: GetOrderTransactionOptions) => void;

export interface GetOrderTransactionOptions extends GetOrderTransactionContextOptions, OrderTransactionBaseOptions {}

export interface GetOrderTransactionsOptions extends GetOrderTransactionsContextOptions, OrderTransactionBaseOptions {}

export interface UpdateOrderTransactionContextOptions extends GetOrderTransactionContextOptions, OrderTransactionBaseOptions, TransactionPayload {}

export type CreateContextOrderTransactionFunction = (options: CreateOrderTransactionOptions) => void;

export interface CreateOrderTransactionContextOptions extends OrderTransactionBaseOptions, TransactionPayload {
    Created: TransactionCallBack;
}

// noinspection JSUnusedGlobalSymbols
export interface DeleteOrderTransactionOptions extends OrderTransactionBaseOptions {
    OK: TransactionDeleteCallBack;
}

export interface CreateOrderTransactionOptions extends BaseOptions, CreateOrderTransactionContextOptions {}

// noinspection JSUnusedGlobalSymbols
export interface UpdateOrderTransactionOptions extends OrderTransactionBaseOptions, UpdateOrderTransactionContextOptions {
    id: number;
}

export type GetContextOrderTransactionsFunction = (options: GetContextOrderTransactionsOptions) => void;

export type GetOrderTransactions = (transactions: Transaction[]) => void;

export interface GetContextOrderTransactionsOptions extends BaseOptions {
    since_id?: number; // Retrieve only transactions after the specified ID.
    fields?: string; // Show only certain fields, specifed by a comma-separated list of fields names.
    OK: GetOrderTransactions;
}

export interface GetOrderTransactionsOptions extends GetContextOrderTransactionsOptions, OrderBaseOptions {}

export type GetContextOrderTransactionCountFunction = (options: GetOrderTransactionCountOptions) => void;

export type GetOrderTransactionCount = (count: number) => void;

export interface GetOrderTransactionCountOptions extends OrderBaseOptions {
    OK: GetOrderTransactionCount;
}
