/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {AssignedLocation} from './AssignedLocation';
import {Destination} from './Destination';

export interface FulfillmentOrder {
    id: number;
    shop_id: number;
    order_id: number;
    assigned_location_id: number;
    request_status: string;
    status: string;
    supported_actions: string[];
    destination: Destination;
    line_items: FulfillmentOrderLineItem[];
    fulfillment_service_handle: string;
    assigned_location: AssignedLocation;
    merchant_requests: string[];
}

export interface FulfillmentOrderLineItem {
    id: number;
    shop_id: number;
    fulfillment_order_id: number;
    quantity: number;
    line_item_id: number;
    inventory_item_id: number;
    fulfillable_quantity: number;
    variant_id: number;
}
