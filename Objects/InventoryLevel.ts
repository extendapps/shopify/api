/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface InventoryLevel {
    available: number;
    inventory_item_id: number;
    location_id: number;
    updated_at: string; // '2012-02-15T15:12:21-05:00'
}
