/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Base} from './Base';

export interface InventoryItem extends Base {
    sku: string;
    updated_at: string;
    requires_shipping: boolean;
    cost: string;
    country_code_of_origin: string;
    province_code_of_origin: string;
    harmonized_system_code: string;
    tracked: boolean;
    country_harmonized_system_codes: string[];
    admin_graphql_api_id: string;
}
