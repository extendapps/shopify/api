/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import * as https from 'N/https';

export interface BaseOptions {
    Failed?: FailedCallback;
}

export type FailedCallback = (clientResponse: https.ClientResponse) => void;

export interface BaseSearchOptions {
    ids?: number[]; // Retrieve only orders specified by a comma-separated list of order IDs.
    limit?: number; // The maximum number of results to show on a page. (default: 50, maximum: 250)
    since_id?: number; // Show orders after the specified ID.
    created_at_min?: string; // Show orders created at or after date (format: 2014-04-25T16:15:47-04:00).
    created_at_max?: string; // Show orders created at or before date (format: 2014-04-25T16:15:47-04:00).
    updated_at_min?: string; // Show orders last updated at or after date (format: 2014-04-25T16:15:47-04:00).
    updated_at_max?: string; // Show orders last updated at or before date (format: 2014-04-25T16:15:47-04:00).
    fields?: string[]; // Retrieve only certain fields, specified by a comma-separated list of fields names.
}
