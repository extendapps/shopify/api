/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface DiscountApplication {
    type: 'manual' | 'script' | 'discount_code';
    value: string;
    value_type: 'fixed_amount' | 'percentage';
    allocation_method: 'across' | 'each' | 'one';
    target_selection: 'all' | 'entitled' | 'explicit';
    target_type: 'line_item' | 'shipping_line';
    description: string;
    title: string;
    code: string;
}
