/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {Address} from '../Objects/Address';

export type AddressCallBack = (address: Address) => void;
export type AddressesCallBack = (addresses: Address[]) => void;
export type CustomerAddressCallBack = (customerAddress: Address) => void;

export interface AddressPayload {
    address: Address;
}

export interface AddressesPayload {
    addresses: Address[];
}
