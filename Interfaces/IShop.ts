/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions} from './Base';
import {Shop} from '../Objects/Shop';

export type GetShopPropertiesCallback = (shop: Shop) => void;

export interface ShopPropertiesPayload {
    shop: Shop;
}

export interface GetShopPropertiesOptions extends BaseOptions {
    OK: GetShopPropertiesCallback;
}
