/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */
import {BaseOptions} from '../Interfaces/Base';

export interface Location {
    id: number;
    name: string;
    address1: string;
    address2: string;
    city: string;
    zip: string;
    province: string;
    country: string;
    phone: string;
    created_at: Date;
    updated_at: Date;
    country_code: string;
    country_name: string;
    province_code: string;
    legacy: boolean;
    active: boolean;
    admin_graphql_api_id: string;
    localized_country_name: string;
    localized_province_name: string;
}

export interface GetLocationsOptions extends BaseOptions {
    OK: GetLocationsCallback;
}

export type GetLocationsCallback = (locations: Location[]) => void;

export interface GetLocationsResponse {
    locations: Location[];
}
