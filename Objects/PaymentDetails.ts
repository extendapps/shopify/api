/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface PaymentDetails {
    credit_card_bin?: string;
    avs_result_code?: string; // single letter
    cvv_result_code?: string;
    credit_card_number: string;
    credit_card_company: string;
}
