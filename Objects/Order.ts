/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {FinancialStatus, FulfillmentStatus} from '../Interfaces/ShopifyTypes';
import {DiscountApplication} from './DiscountApplication';
import {DiscountCode} from './DiscountCode';
import {LineItem} from './LineItem';
import {LatLongAddress} from './Address';
import {Fulfillment} from './Fulfillment';
import {Customer} from './Customer';
import {CreateContextOrderTransactionFunction, GetContextOrderTransactionCountFunction, GetContextOrderTransactionFunction, GetContextOrderTransactionsFunction} from '../Interfaces/IOrderTransaction';
import {GetContextOrderRefundFunction, GetContextOrderRefundsFunction} from '../Interfaces/IOrderRefund';
import {ShippingLine} from './ShippingLine';
import {TaxLine} from './TaxLine';
import {Base} from './Base';

export interface Order extends Base {
    email: string;
    closed_at?: string;
    updated_at: string; // '2012-02-15T15:12:21-05:00'
    number: number;
    note?: string;
    token: string;
    gateway?: string;
    test: boolean;
    total_price: string;
    subtotal_price: string;
    total_weight: number;
    total_tax: string;
    taxes_included: boolean;
    currency: string;
    financial_status: FinancialStatus;
    confirmed: boolean;
    total_discounts: string;
    total_line_items_price: string;
    cart_token?: string;
    buyer_accepts_marketing: boolean;
    name: string;
    referring_site?: string;
    landing_site?: string;
    cancelled_at: string; // '2012-02-15T15:12:21-05:00'
    cancel_reason: 'customer' | 'fraud' | 'inventory' | 'declined' | 'other';
    total_price_usd?: string;
    checkout_token?: string;
    reference?: string;
    user_id?: string;
    location_id?: number;
    source_identifier?: string;
    source_url?: string;
    processed_at?: string;
    device_id?: string;
    phone?: string;
    customer_locale: string;
    app_id?: string;
    browser_ip?: string;
    landing_site_ref?: string;
    order_number: number;
    discount_applications: DiscountApplication[];
    discount_codes: DiscountCode[];
    note_attributes: {name?: string; value?: string}[];
    payment_gateway_names: string[];
    processing_method: 'checkout' | 'direct' | 'manual' | 'offsite' | 'express';
    checkout_id?: string;
    source_name: string;
    fulfillment_status: FulfillmentStatus;
    tax_lines: TaxLine[];
    tags: string;
    contact_email: string;
    order_status_url: string;
    line_items: LineItem[];
    shipping_lines: ShippingLine[];
    billing_address: LatLongAddress;
    shipping_address: LatLongAddress;
    fulfillments: Fulfillment[];
    refunds: string[];
    customer: Customer;

    getTransactions?: GetContextOrderTransactionsFunction;
    getTransaction?: GetContextOrderTransactionFunction;
    getTransactionCount?: GetContextOrderTransactionCountFunction;
    getRefunds?: GetContextOrderRefundsFunction;
    getRefund?: GetContextOrderRefundFunction;
    createTransaction?: CreateContextOrderTransactionFunction;
}
