/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

import {BaseOptions, BaseSearchOptions} from './Base';
import {Product} from '../Objects/Product';

export type ProductCallback = (product: Product) => void;
export type ProductsCallback = (products: Product[]) => void;

export interface ProductPayload {
    product: Product;
}

export interface ProductsPayload {
    products: Product[];
}

export interface ProductBaseOptions extends BaseOptions {
    product_id: number;
}

// tslint:disable-next-line:no-empty-interface
export type ProductsBaseOptions = BaseOptions;

export interface GetProductsBaseOptions extends ProductsBaseOptions, BaseSearchOptions {
    title?: string;
    vendor?: string;
    handle?: string;
    product_type?: string;
    collection_id?: string;
}

export interface GetProductOptions extends ProductBaseOptions {
    OK: ProductCallback;
}

export interface GetProductsOptions extends GetProductsBaseOptions {
    OK: ProductsCallback;
}

export interface CreateProductOptions extends BaseOptions, ProductPayload {
    Created: ProductCallback;
}

export interface UpdateProductOptions extends ProductPayload, BaseOptions {
    OK: ProductCallback;
}

export interface DeleteProductOptions extends BaseOptions {
    product_id: number;

    Deleted(): void;
}
