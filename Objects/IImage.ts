/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 */

export interface IImage {
    id: number;
    product_id: number;
    position: number;
    created_at: string; // '2012-02-15T15:12:21-05:00'
    updated_at: string; // '2012-02-15T15:12:21-05:00'
    alt?: string;
    width: number;
    height: number;
    src: string;
    variant_ids: number[];
    admin_graphql_api_id: string;
}
